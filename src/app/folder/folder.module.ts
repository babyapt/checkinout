import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FolderPageRoutingModule } from './folder-routing.module';

import { FolderPage } from './folder.page';
import { MainMenuComponent } from '../shared/main-menu/main-menu.component';

import { ProfileComponent } from '../shared/profile/profile.component'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FolderPageRoutingModule
  ],
  declarations: [FolderPage, ProfileComponent, MainMenuComponent]
})
export class FolderPageModule { }
