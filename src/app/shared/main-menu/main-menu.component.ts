import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class MainMenuComponent implements OnInit {
  menu:any = new Array();
  km_data:any = '..กำลังประมวลผล';
  km_cal:any = "disabled";
  let_master:any = '13.6899991';
  long_master:any = '100.7479183';
  interval:any;
  constructor
  (
    private geolocation: Geolocation,
    public alertController: AlertController,
    private router: Router
  ) 
  { 
    this.set_inter();
    this.menu = [
      {
        btn_name: 'บันทึกเวลา (Out Side)',
        icon: 'airplane-outline',
        show_roud: false,
        function: '',
      },
      {
        btn_name: 'ตรวจสอบการ Checkin',
        icon: 'calendar-outline',
        show_roud: false,
        function: '1',
      }
    ];
  }

  ngOnInit() {}

  set_inter(){
    this.interval = setInterval(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
        // console.log(resp.coords.latitude);
        // console.log(resp.coords.longitude);
        this.cal_lat_long(resp.coords.latitude,resp.coords.longitude);
      }).catch((error) => {
        console.log('Error getting location', error);
        clearInterval(this.interval);
        setTimeout(() => {
          this.set_inter();
        }, 60000);
      });
    }, 3000);
  }

  cal_lat_long(let_now,long_now){
    return new Promise(resolve => {
      if(this.check_empty(let_now) && this.check_empty(long_now)){
        var cal_res:any = this.getDistanceFromLatLonInKm(this.let_master,this.long_master,let_now,long_now);
        this.km_data = '('+parseFloat(cal_res.toFixed(2))+' Km)';
        if(parseFloat(cal_res.toFixed(2)) > 0.10){
          this.km_cal = "disabled";
        }else{
          this.km_cal = '';
        }
        // console.log("this.km_cal : "+this.km_cal);
      }else{
        clearInterval(this.interval);
        setTimeout(() => {
          this.set_inter();
        }, 60000);
      }
    });
  }

  check_empty(data:any){
      if(data !== '' && data !== null && typeof data !== "undefined"){
        return true;
      }else{
        return false;
      }
  }

  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }
  
  deg2rad(deg) {
    return deg * (Math.PI/180);
  }

  check_in(){
    if(this.km_cal != "disabled"){
      this.presentAlert();
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'แจ้งเตือน',
      subHeader: 'ต้องการลงเวลาทำงานใช่หรือไม่?',
      message: '',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'ตกลง',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  save_check_in(){

  }

  go_to_check_time_page(){
    this.router.navigateByUrl('check-time-list');
  }

}
