import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckTimeListPage } from './check-time-list.page';

const routes: Routes = [
  {
    path: '',
    component: CheckTimeListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckTimeListPageRoutingModule {}
