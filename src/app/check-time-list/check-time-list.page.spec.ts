import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckTimeListPage } from './check-time-list.page';

describe('CheckTimeListPage', () => {
  let component: CheckTimeListPage;
  let fixture: ComponentFixture<CheckTimeListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckTimeListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckTimeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
