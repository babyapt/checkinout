import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckTimeListPageRoutingModule } from './check-time-list-routing.module';

import { CheckTimeListPage } from './check-time-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckTimeListPageRoutingModule
  ],
  declarations: [CheckTimeListPage]
})
export class CheckTimeListPageModule {}
